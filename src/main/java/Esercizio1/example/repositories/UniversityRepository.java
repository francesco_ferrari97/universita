package Esercizio1.example.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import Esercizio1.example.classes.State;
import Esercizio1.example.classes.University;

@Repository
public interface UniversityRepository extends PagingAndSortingRepository<University, Long> {
	
	University findFirstById(Long id);
	
	University findFirstByName(String name);
	
	University findFirstByNumeroIscritti(Long numeroIscritti);
	
	University findFirstByNameAndIdIsNot(String name, Long id);
	
	@Query(value = "SELECT uni " + 
			"from University uni " +
			"where (uni.id = :id or :id is null) " +
			"and (uni.name = :name or :name is null) " +
			"and (uni.numeroIscritti = :numeroIscritti or :numeroIscritti is null) " +
			"and (uni.state.code = :stateCode or :stateCode is null) ")
	Page<University> findQuery(@Param("id") Long id,
			                   @Param("name") String name,
			                   @Param("numeroIscritti") Long numeroIscritti,
			                   @Param("stateCode") String idState,
			                   Pageable pageable);
	
	
	
}





// "join State s on uni.stateId=s.id " +
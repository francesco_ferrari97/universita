package Esercizio1.example.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import Esercizio1.example.classes.Materia;

@Repository
public interface MateriaRepository extends PagingAndSortingRepository<Materia, Long>{
	
	Materia findFirstById(Long id);
	
	Materia findFirstByNomeMateria(String nomeMateria);
	
	Materia findFirstByMateriaPropedeutica(String materiaPropedeutica);
	
	Materia findFirstByCfu(Integer cfu);

	Materia findFirstByCodiceMateria(String codiceMateria);
	
	Materia findFirstByCodiceMateriaAndIdIsNot(String codiceMateria, Long id);

	Page <Materia> findAllBycodiceMateriaLike(String codiceMateria, Pageable page);

	@Query(value = "SELECT mat " + 
			"from Materia mat " +
			"where (mat.id = :id or :id is null) " +
			"and (mat.nomeMateria = :nomeMateria or :nomeMateria is null) " +
			"and (mat.codiceMateria = :codiceMateria or :codiceMateria is null) " +
			"and (mat.materiaPropedeutica LIKE :materiaPropedeutica or :materiaPropedeutica is null) " + 
			"and (mat.cfu = :cfu or :cfu is null) ")
	Page<Materia> findQuery(@Param("id") Long id,
			                @Param("nomeMateria") String nomeMateria,
			                @Param("codiceMateria") String codiceMateria,
			                @Param("materiaPropedeutica") String materiaPropedeutica,
			                @Param("cfu") Integer cfu,
			                Pageable pageable);
	
	

}

package Esercizio1.example.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import Esercizio1.example.classes.Segue;

@Repository
public interface SegueRepository extends PagingAndSortingRepository<Segue, Long> {

	void deleteByIdCorsoAndIdStudent(Long idCorso, Long idStudent);

	Segue findByIdCorsoAndIdStudent(Long idCorso, Long idStudent);
	
	Segue findFirstByIdStudent(Long idStudent);
	
	Segue findFirstByIdCorso(Long idCorso);
	
	Segue findFirstById(Long id);
	
	void deleteById(Long id);


	@Query(value = "SELECT seg " + 
				   "from Segue seg " +
				   "where (seg.idStudent = :idStudent or :idStudent is null) " +
				   "and (seg.idCorso = :idCorso or :idCorso is null) ")
	Page<Segue> findQuery(@Param("idStudent") Long idStudent,
						  @Param("idCorso") Long idCorso,		   
						  Pageable pageable);




}

package Esercizio1.example.repositories;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import Esercizio1.example.classes.Libretto;


@Repository
public interface LibrettoRepository extends PagingAndSortingRepository<Libretto, Long> {
	
	Libretto findFirstById(Long id);
	
	Libretto findFirstByData(LocalDate dataSostenimento);
	
	Libretto findFirstByVoto(Integer voto);
	
	//Libretto findRecord(String matricola, String codiceMateria, String nomeCorso,Integer voto, LocalDate dataSostenimento);

	
	//Nel metodo salva vedere:
	/*2)
	 * Seleziona la variabile lib 
	 * Da libretto, dove l'id di lib.student.id è uguale a l'id che sto passando da Param.
	 * dove l'id di lib.corso.id è uguale a l'id che sto passando da Param
	 * dove l'id di lib.materia.id è uguale a l'id che sto passando da Param
	 * */
	@Query (value = "SELECT lib " +
			"FROM Libretto lib " +
			"WHERE (lib.student.id = :studentId) " +
			"AND (lib.corso.id = :corsoId) " +
			"AND (lib.materia.id = :materiaId) " 
			)
	Libretto findExam(@Param("studentId") Long idStudent,
					  @Param("corsoId") Long idCorso,
					  @Param("materiaId") Long idMateria);
	
	
	@Query(value = "SELECT lib " +
			"FROM Libretto lib " +
			"WHERE (lib.materia = :propedeutica) " +
			"AND (lib.student.id = :studentId) " +
			"AND (lib.corso.id = :corsoId) "
			)
	Libretto findExamPassed(@Param("propedeutica")String propedeutica, 
							@Param("studentId")Long idStudent, 
							@Param("corsoId")Long idCorso);

	
	@Query(value = "SELECT l " + 
			"from Libretto l " +
			"where (l.id = :id or :id is null) " +
			"and (l.student.matricola = :student or :student is null) " +
			"and (l.materia.codiceMateria = :materia or :materia is null) " +
			"and (l.corso.nome = :corso or :corso is null) " + 
			"and (l.dataSostenimento = :dataSostenimento or :dataSostenimento is null) " + 
			"and (l.voto = :voto or :voto is null) ")
	Page<Libretto> findQuery(@Param("id") Long id,
			              	 @Param("student")String matricola,
			              	 @Param("materia")String codiceMateria,
			              	 @Param("corso")String nomeCorso,
			              	 @Param("dataSostenimento") LocalDate dataSostenimento,
			              	 @Param("voto") Integer voto,
			              	 Pageable pageable);

	
}

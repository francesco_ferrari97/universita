package Esercizio1.example.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import Esercizio1.example.classes.Corso;

@Repository
public interface CorsoRepository extends PagingAndSortingRepository<Corso, Long> {
	
	Corso findFirstById(Long id);
	
	Corso findFirstByAmbito(String ambito);
	
	Corso findFirstByDurata(Integer durata);
	
	Corso findFirstByNome(String name);

	Corso findFirstByNomeAndUniversityName(String nome, String nomeUniversity);
	
	Corso findFirstByNomeAndUniversityNameAndIdIsNot(String nome, String nomeUniversity, Long idCorso);
	
	Corso findFirstByNomeAndIdIsNot(String name, Long id);
	
	Page <Corso> findAllByMaterieLike(String materieStr, Pageable page);
	
	
	@Query(value = "SELECT c " + 
			"from Corso c " +
			"where (c.id = :id or :id is null) " +
			"and (c.nome = :nome or :nome is null) " +
			"and (c.ambito = :ambito or :ambito is null) " +
			"and (c.durata = :durata or :durata is null) " )
	Page<Corso> findQuery(@Param("id") Long id,
			              @Param("nome") String nome,
			              @Param("ambito") String ambito,
			              @Param("durata") Integer durata,
			              Pageable pageable);


	
	
	
}

package Esercizio1.example.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import Esercizio1.example.classes.State;


@Repository
public interface StateRepository extends PagingAndSortingRepository<State, Long>{
	
	State findFirstById(Long id);
	
	State findFirstByName(String name);
	
	State findFirstByPopulation(Long population);

	State findFirstByCode(String code);
	
	State findFirstByCodeAndIdIsNot(String code, Long id);


	@Query(value = "SELECT sta " + 
			"from State sta " +
			"where (sta.id = :id or :id is null) " +
			"and (sta.name = :name or :name is null) " +
			"and (sta.code LIKE :code or :code is null) " +
			"and (sta.population = :population or :population is null) ")
		 Page<State> findQuery(@Param("id") Long id,
			                   @Param("name") String name,
			                   @Param("code") String code,
			                   @Param("population") Long population,
			                   Pageable pageable);


}

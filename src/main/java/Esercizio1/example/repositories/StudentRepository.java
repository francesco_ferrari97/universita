package Esercizio1.example.repositories;
import Esercizio1.example.classes.*;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends PagingAndSortingRepository<Student,Long> {
	
	Student findFirstById(Long id);
	
	Student findFirstByName(String name);
	
	Student findFirstByLastName(String lastName);

	Student findFirstByMatricola(String matricola);
	
	Student findFirstByDataNascita(LocalDate dataNascita);

	Student findFirstByDataIscrizione(LocalDate dataIscrizione);
	
	Student findFirstByMatricolaAndIdIsNot(String matricola, Long id);

	Page <Student> findAllByMatricolaLike(String matricola, Pageable page);

	@Query(value = "SELECT stud " + 
			"from Student stud " +
			"where (stud.id = :id or :id is null) " +
			"and (stud.name = :name or :name is null) " +
			"and (stud.lastName = :lastName or :lastName is null) " +
			"and (stud.matricola LIKE :matricola or :matricola is null) " + 
			"and (stud.dataNascita = :dataNascita or :dataNascita is null) " +
			"and (stud.dataIscrizione = :dataIscrizione or :dataIscrizione is null) "
			)
	Page<Student> findQuery(@Param("id") Long id,
			                @Param("name") String name,
			                @Param("lastName") String lastName,
			                @Param("matricola") String matricola,
			                @Param("dataNascita") LocalDate dataNascita,
			                @Param("dataIscrizione") LocalDate dataIscrizone,
			                Pageable pageable);

	
	
}

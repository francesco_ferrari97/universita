package Esercizio1.example.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import Esercizio1.example.classes.Contiene;

@Repository
public interface ContieneRepository extends PagingAndSortingRepository<Contiene, Long> {

	Contiene deleteByIdCorsoAndIdMateria(Long idCorso, Long idMateria);

	Contiene findByIdCorsoAndIdMateria(Long idCorso, Long idMateria);
	
	Contiene findFirstByIdMateria(Long idMateria);
	
	Contiene findFirstByIdCorso(Long idCorso);
	
	Contiene findFirstById(Long id);
	
	void deleteById(Long id);


	@Query(value = "SELECT con " + 
				   "from Contiene con " +
				   "where (con.idMateria = :idMateria or :idMateria is null) " +
				   "and (con.idCorso = :idCorso or :idCorso is null) ")
	Page<Contiene> findQuery(@Param("idMateria") Long idMateria,
					      @Param("idCorso") Long idCorso,		   
					      Pageable pageable);

}

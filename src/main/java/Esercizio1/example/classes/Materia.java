package Esercizio1.example.classes;

import java.util.Optional;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import Esercizio1.example.exceptions.BadRequestException;

@Entity
@Table
public class Materia {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="nome")
	private String nomeMateria;
	
	@Column(name="codice")
	private String codiceMateria;
	
	@Column(name="propedeutica")
	private String materiaPropedeutica;
	
	@Column(name="cfu")
	private Integer cfu;
	
	
	public void validateMateria() throws BadRequestException{
		
		this.nomeMateria = Optional.ofNullable(this.nomeMateria).orElseThrow(()-> new BadRequestException("Il nome della materia è obbligatorio"));
		
		this.cfu = Optional.ofNullable(this.cfu).orElseThrow(() -> new BadRequestException("I CFU sono obbligatori."));
		
		if(this.cfu!=3 && this.cfu!=6 && this.cfu!=9 && this.cfu!=12) {
			throw new BadRequestException("I CFU possono avere solo valori come: 3,6,9,12.");
		} 
		
		this.codiceMateria = Optional.ofNullable(this.codiceMateria).orElseThrow(() -> new BadRequestException("Il codice della materia è obbligatorio."));
	}

	public Materia() {
	}

	public Materia(Long id, String nomeMateria, String codiceMateria, String materiaPropedeutica, Integer cfu) {
		super();
		this.id = id;
		this.nomeMateria = nomeMateria;
		this.codiceMateria = codiceMateria;
		this.materiaPropedeutica = materiaPropedeutica;
		this.cfu = cfu;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeMateria() {
		return nomeMateria;
	}

	public void setNomeMateria(String nomeMateria) {
		this.nomeMateria = nomeMateria;
	}

	public String getCodiceMateria() {
		return codiceMateria;
	}

	public void setCodiceMateria(String codiceMateria) {
		this.codiceMateria = codiceMateria;
	}

	public String getMateriaPropedeutica() {
		return materiaPropedeutica;
	}

	public void setMateriaPropedeutica(String materiaPropedeutica) {
		this.materiaPropedeutica = materiaPropedeutica;
	}

	public int getCfu() {
		return cfu;
	}

	public void setCfu(Integer cfu) {
		this.cfu = cfu;
	}
	
	
	

}

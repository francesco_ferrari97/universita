package Esercizio1.example.classes;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import Esercizio1.example.exceptions.BadRequestException;

@Entity
@Table
public class Libretto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "student_id")
	private Student student;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "materia_id")
	private Materia materia;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "corso_id")
	private Corso corso;
	
	@Column(name="dataSostenimento")
	@JsonSerialize
	@JsonDeserialize
	@JsonFormat(pattern = "dd/MM/yyyy") 
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataSostenimento;
	
	@Column(name="voto")
	private Integer voto;
	

	
	
	
	public void validateLibretto() throws BadRequestException {
		
		if(this.voto == null) {
			throw new BadRequestException(String.format("Il voto è obbligatorio %s", this.voto));
		}
		
		
		if(this.voto>31 || this.voto<0) {
			throw new BadRequestException(String.format("Registrazione voto non valido %s", this.voto));
		}
		
		
		this.dataSostenimento = Optional.ofNullable(this.dataSostenimento)
										.orElseThrow(()-> new BadRequestException(String.format("La data è obbligatoria", this.dataSostenimento)));
				
		if(this.student == null) {
			throw new BadRequestException(String.format("Lo studente è obbligatorio %s", this.student));
		}
		
		
		if(this.materia == null) {
			throw new BadRequestException(String.format("La materia è obbligatoria %s", this.materia));
		}
		
		if(this.corso == null) {
			throw new BadRequestException(String.format("Il Corso è obbligatoria %s", this.corso));
		}
	}
	
	public Libretto() {
	}

	public Libretto(Long id, Materia materia, Corso corso, LocalDate dataSostenimento, Integer voto) {
		super();
		this.id = id;
		this.materia = materia;
		this.corso = corso;
		this.dataSostenimento = dataSostenimento;
		this.voto = voto;
	}


	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}
	
	public Corso getCorso() {
		return corso;
	}


	public void setCorso(Corso corso) {
		this.corso = corso;
	}


	public LocalDate getDataSostenimento() {
		return dataSostenimento;
	}


	public void setDataSostenimento(LocalDate dataSostenimento) {
		this.dataSostenimento = dataSostenimento;
	}


	public int getVoto() {
		return voto;
	}

	public void setVoto(Integer voto) {
		this.voto = voto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	

}

package Esercizio1.example.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import Esercizio1.example.exceptions.BadRequestException;

@Entity
@Table
public class State {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="nome")
	private String name;
	
	@Column(name="codice")
	private String code;
	
	@Column(name="popolazione")
	private Long population;
	
	
	
	public void validateState() throws BadRequestException {
		if(this.code == null) {
			throw new BadRequestException("Il codice è obbligatorio.");
		} 
		
		if(this.name == null) {
			throw new BadRequestException("Il nome è obbligatorio");
		}
	}
	
	public State(Long id, String name, String code, Long population) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.population = population;
	}
	
	public State() {
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getPopulation() {
		return population;
	}


	public void setPopulation(Long population) {
		this.population = population;
	}
	
	
	
	
}
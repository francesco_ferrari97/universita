package Esercizio1.example.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import Esercizio1.example.exceptions.BadRequestException;

@Entity
@Table
public class University {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="nome")
	private String name;
	
	@Column(name="iscritti")
	private Long numeroIscritti;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "state_id")
	private State state;

	
	public University(Long id, String name, Long numeroIscritti,State state) {
		super();
		this.id = id;
		this.name = name;
		this.numeroIscritti = numeroIscritti;
		this.state = state;
	}
	
	public void validateUniversity(Long isc) throws BadRequestException{
		
		if(this.name == null)
		throw new BadRequestException("Il nome è obbligatorio.");
		
		if(this.numeroIscritti > isc) {
			throw new BadRequestException(String.format("Il numero massimo per gli iscritti è : %s", isc));
		}
	}
	
	public University () {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNumeroIscritti() {
		return numeroIscritti;
	}

	public void setNumeroIscritti(Long numeroIscritti) {
		this.numeroIscritti = numeroIscritti;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}


	
	

	

	
	
	
	
}
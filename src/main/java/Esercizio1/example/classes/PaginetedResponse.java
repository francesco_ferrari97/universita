package Esercizio1.example.classes;

import java.util.List;

public class PaginetedResponse <T> {
	
	private Integer page;
	private Long total;
	private List<T> items;
	
	public PaginetedResponse(Integer page, Long total, List<T> items) {
		super();
		this.page = page;
		this.total = total;
		this.items = items;
	}

	
	public PaginetedResponse() {
		super();
	}


	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}
	
	
	
	
	
	
	
	
	
}

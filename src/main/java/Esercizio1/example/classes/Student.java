package Esercizio1.example.classes;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import Esercizio1.example.exceptions.BadRequestException;


@Entity
@Table
public class Student {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="nome")
	private String name;
	
	@Column(name="cognome")
	private String lastName;
	
	@Column(name="matricola")
	private String matricola;
	
	@Column(name="dataIscrizione")
	@JsonSerialize
	@JsonDeserialize
	@JsonFormat(pattern = "dd/MM/yyyy") 
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataIscrizione;
	
	@Column(name="dataNascita")
	@JsonSerialize
	@JsonDeserialize
	@JsonFormat(pattern = "dd/MM/yyyy") 
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataNascita;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "university_id")
	private University university;
	
	@JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name ="segue",  
                joinColumns = { @JoinColumn(name = "student_id") }, 
                inverseJoinColumns = { @JoinColumn(name = "corso_id") }
    			)
    private List<Corso> corso;
	

	public void validateStudent() throws BadRequestException{
		LocalDate today = LocalDate.now();
		int year = today.getYear();
		
		this.dataIscrizione = Optional.ofNullable(this.dataIscrizione).orElse(LocalDate.now());
		
		if(this.matricola == null)
		throw new BadRequestException("La matricola è obbligatoria.");
		
		if(year - Optional.ofNullable(this.dataNascita).map(b -> b.getYear()).orElse(year)<18) {
		throw new BadRequestException("Lo studente deve essere maggiorenne.");
		}
		
		if(dataIscrizione.isAfter(LocalDate.now())) {
		throw new BadRequestException("La data d'iscrizione deve essere minore della data odierna.");
		}
		
		if(this.name == null) {
		throw new BadRequestException("Lo studente deve avere il nome.");
		}
		
		if(this.lastName == null) {
		throw new BadRequestException("Lo studente deve avere il cognome.");
		}
	}
	
	public Student() {
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMatricola() {
		return matricola;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	public LocalDate getDataIscrizione() {
		return dataIscrizione;
	}

	public void setDataIscrizione(LocalDate dataIscrizione) {
		this.dataIscrizione = dataIscrizione;
	}

	public LocalDate getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(LocalDate dataNascita) {
		this.dataNascita = dataNascita;
	}

	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	public List<Corso> getCorso() {
		return corso;
	}

	public void setCorso(List<Corso> corso) {
		this.corso = corso;
	}

	
	
	
	
	
	

}

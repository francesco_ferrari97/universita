package Esercizio1.example.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Segue {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="student_id")
	private Long idStudent;
	
	@Column(name="corso_id")
	private Long idCorso;
	

	public Segue() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdStudent() {
		return idStudent;
	}

	public void setIdStudent(Long idStudent) {
		this.idStudent = idStudent;
	}

	public Long getIdCorso() {
		return idCorso;
	}

	public void setIdCorso(Long idCorso) {
		this.idCorso = idCorso;
	}
	
	

	

}

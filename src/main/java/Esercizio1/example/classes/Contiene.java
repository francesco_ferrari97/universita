package Esercizio1.example.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Contiene {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="materia_id")
	private Long idMateria;
	
	@Column(name="corso_id")
	private Long idCorso;

	public Contiene(Long id, Long idMateria, Long idCorso) {
		super();
		this.id = id;
		this.idMateria = idMateria;
		this.idCorso = idCorso;
	}

	public Contiene() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long cor) {
		this.id = cor;
	}

	public Long getIdMateria() {
		return idMateria;
	}

	public void setIdMateria(Long idMateria) {
		this.idMateria = idMateria;
	}

	public Long getIdCorso() {
		return idCorso;
	}

	public void setIdCorso(Long cor) {
		this.idCorso = cor;
	}

	
	
	
	
	
	
	
	
	
}

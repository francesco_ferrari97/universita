package Esercizio1.example.classes;


import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import Esercizio1.example.exceptions.BadRequestException;

@Entity
@Table
public class Corso {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "nome")
	private String nome;
	@Column(name = "ambito")
	private String ambito;
	@Column(name = "durata")
	private Integer durata;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="university_id")
	private University university;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable( name ="contiene",  
	             joinColumns = { @JoinColumn(name = "corso_id") }, 
	             inverseJoinColumns = { @JoinColumn(name = "materia_id") }
	    		)
	private List<Materia> materie;
	
	
	public void validateCorso(String scientifico, String umanitario, String professionale) throws BadRequestException {
		
		if(this.ambito!=null) {
		if(!this.ambito.equals(scientifico) && !this.ambito.equals(umanitario) && !this.ambito.equals(professionale)) {
			throw new BadRequestException("Ambito non valido");
		}
		}
		
		if(this.nome == null) {
			throw new BadRequestException(String.format("Nome corso obbligatorio "));
		}
		
		if(this.ambito == null) {
			throw new BadRequestException(String.format("Ambito del corso obbligatorio"));
		}
		
		if(this.durata<2 || this.durata>5) {
			throw new BadRequestException(String.format("La durata del corso devere essere maggiore di 2 o minore di 5."));
		}
		
	}
	
	/*public void materieStringToList() {
		if(this.materieStr != null) {
			this.materie = Arrays.asList(this.materieStr.split(","));
		}
	}
	

	public void materieListToString() {
		if(this.materie != null && !this.materie.isEmpty()) {
			this.materieStr = this.materie.stream().collect(Collectors.joining(","));
		}
	}*/
	
	public Corso() {
	
	}


	public Corso(Long id, String nome, String ambito, Integer durata,List<Materia> materie, University university) {
		super();
		this.id = id;
		this.nome = nome;
		this.ambito = ambito;
		this.durata = durata;
		this.materie = materie;
		this.university = university;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getAmbito() {
		return ambito;
	}


	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}


	public int getDurata() {
		return durata;
	}


	public void setDurata(Integer durata) {
		this.durata = durata;
	}



	public University getUniversity() {
		return university;
	}


	public void setUniversity(University university) {
		this.university = university;
	}

	public List<Materia> getMaterie() {
		return materie;
	}

	public void setMaterie(List<Materia> materie) {
		this.materie = materie;
	}
	
	
	
	
	

}

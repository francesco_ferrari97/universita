package Esercizio1.example.services;


import java.util.List;

import Esercizio1.example.classes.Contiene;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;

public interface ContieneService {
	
	
	Contiene findByIdContiene(Long id) throws NotFoundException, BadRequestException;
	public PaginetedResponse<Contiene> findListContains(Long idMateria, Long idCorso, Integer page, Integer limit, String sort) throws BadRequestException,NotFoundException ;
	Contiene save(Contiene c) throws NotFoundException, BadRequestException, ConflictException;
	void delete(Long id) throws NotFoundException;
	Contiene findByIdCorsoAndIdMateria(Long idCorso, Long idMateria) throws NotFoundException,BadRequestException;
	List<Contiene> insertAll(List<Contiene> associazioni) throws BadRequestException, ConflictException, NotFoundException;
	

}

package Esercizio1.example.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import Esercizio1.example.classes.Corso;
import Esercizio1.example.classes.Materia;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.State;
import Esercizio1.example.classes.University;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.repositories.CorsoRepository;
import Esercizio1.example.repositories.UniversityRepository;
import Esercizio1.example.utils.Utilities;

@Service
public class CorsoServiceImpl implements CorsoService {
	
	private final CorsoRepository corsoRepository;
	private final UniversityRepository universityRepository;

	@Autowired
	public CorsoServiceImpl(CorsoRepository corsoRepository, UniversityRepository universityRepository) {
		this.corsoRepository = corsoRepository;
		this.universityRepository = universityRepository;
	}

	@Value("${Esercizio1.scientifico}")
	private String scientifico;
	@Value("${Esercizio1.umanitario}")
	private String umanitario;
	@Value("${Esercizio1.professionale}")
	private String professionale;
	@Value("${Esercizio1.default-limit}")
	private Integer defaultLimit;
	@Value("${Esercizio1.max-limit}")
	private Integer maxLimit;

	@Override
	public PaginetedResponse<Corso> findList(Long id, String nome, String ambito,Integer durata, University university, Integer page,Integer limit,String sort) throws BadRequestException, NotFoundException {

		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		page = Optional.ofNullable(page).orElse(0);
		//materieStr = Optional.ofNullable(materieStr).map(m -> "%" + m + "%").orElse(null);
		
		if(nome != null) {
			Corso corsoExist= corsoRepository.findFirstByNome(nome);
			if(corsoExist == null) {
				throw new NotFoundException(String.format("Corso con nome %s non trovato.", nome));
				}
			}
		
		if(ambito != null) {
			Corso corsoExist= corsoRepository.findFirstByAmbito(ambito);
			if(corsoExist == null) {
				throw new NotFoundException(String.format("Corso con ambito %s non trovato.", ambito));
				}
			}
		
		if(durata != null) {
			Corso corsoExist= corsoRepository.findFirstByDurata(durata);
			if(corsoExist == null) {
				throw new NotFoundException(String.format("Corso con durata %s non trovato.", durata));
				}
			}
		
		if(limit > maxLimit)
			throw new BadRequestException(String.format("Il valore massimo per limit è : %s", maxLimit));

		Sort s = Utilities.buildingSort(sort);
		PageRequest pr = null;
		if(s!=null) {
			pr=PageRequest.of(page, limit, s);
		} else {
			pr=PageRequest.of(page, limit);
		}

		Page<Corso>result=corsoRepository.findQuery(id,nome,ambito,durata,pr); 
		//result.getContent().forEach(uni -> uni.materieStringToList());
		return new PaginetedResponse<Corso>(page,result.getTotalElements(),result.getContent());
	}

	@Override
	public Corso findById(Long id) throws NotFoundException, BadRequestException {
		Optional<Corso> corsoExist = corsoRepository.findById(id);//.map(cor -> {cor.materieStringToList(); return cor;});
		return corsoExist.orElseThrow(() -> new NotFoundException(String.format("Corso con id %s non trovato", id)));
	}

	@Override
	public Corso save(Corso corso) throws BadRequestException, ConflictException, NotFoundException {
		corso.validateCorso(scientifico,umanitario,professionale);
		//corso.materieListToString();
		University uni = universityRepository.findFirstByName(corso.getUniversity().getName());
		if(uni==null) {
			throw new NotFoundException("Università inesitente");
		}
		
		corso.setUniversity(uni);
		Corso prev = corsoRepository.findFirstByNomeAndUniversityName(corso.getNome(), corso.getUniversity().getName());
		if(prev != null) {
			throw new ConflictException("Corso già esistente.");
		}
		return corsoRepository.save((corso));
	}

	@Override
	@Transactional
	public Corso update(Long id, Corso corso) throws NotFoundException, BadRequestException, ConflictException {
		
		Corso corsoExist = findById(id);
	
		if(corsoExist != null) {
			corso.setId(id);
			return save(corso);
		} else {
			throw new NotFoundException(String.format("Corso con id %s non trovato", id));
		}
	}

	@Override
	public void delete(Long id) throws NotFoundException {
		
		Corso corsoExist = corsoRepository.findFirstById(id);
		
		if(corsoExist == null) {
			throw new NotFoundException(String.format("Corso con id %s non esiste", id));
		} else {
			corsoRepository.deleteById(id);
		}
	}

	@Override
	public List<Corso> insertAll(List<Corso> corsi) throws BadRequestException, ConflictException, NotFoundException {

		for(Corso c : corsi) {
			University uniExist = universityRepository.findFirstByName(c.getUniversity().getName());
			
			if(uniExist==null) {
				throw new NotFoundException(String.format("L'università con id %s non esiste.", c.getUniversity().getId()));
			}
			
			c.setUniversity(uniExist);
			}
			
			return (List<Corso>) corsoRepository.saveAll(corsi);
	}
	
}

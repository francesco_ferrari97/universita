package Esercizio1.example.services;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.State;
import Esercizio1.example.classes.Student;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.repositories.StateRepository;
import Esercizio1.example.utils.Utilities;


@Service
public class StateServiceImpl implements StateService{

	private final StateRepository stateRepository;

	public StateServiceImpl(StateRepository stateRepository) {
		this.stateRepository = stateRepository;
	}

	@Value("${Esercizio1.default-limit}")
	private Integer defaultLimit;
	@Value("${Esercizio1.max-limit}")
	private Integer maxLimit;

	@Override
	public PaginetedResponse<State> findList(Long id, String name, String code, Long population, Integer page,Integer limit,String sort) throws BadRequestException, NotFoundException {

		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		page = Optional.ofNullable(page).orElse(0);
		
		if(name!=null) {
			State stateExist = stateRepository.findFirstByName(name);
			if(stateExist == null) {
				throw new NotFoundException(String.format("Stato con nome %s non trovato.", name));
			}
		}
		
		
		if(code!=null) {
			State stateExist = stateRepository.findFirstByCode(code);
			if(stateExist == null) {
				throw new NotFoundException(String.format("Stato con codice %s non trovato.", code));
			}
		}
		
		if(population!=null) {
			State stateExist = stateRepository.findFirstByPopulation(population);
			if(stateExist == null) {
				throw new NotFoundException(String.format("Stato con popolazione %s non trovato.", population));
			}
		}
		
		code = Optional.ofNullable(code).map(c-> "%" + c + "%").orElse(null);
		
		if(limit > maxLimit)
			throw new BadRequestException(String.format("Il valore massimo per limit è : %s", maxLimit));

		Sort s = Utilities.buildingSort(sort);
		PageRequest pr = null;
		if(s!=null) {
			pr=PageRequest.of(page, limit, s);
		} else {
			pr=PageRequest.of(page, limit);
		}

		Page<State>result=stateRepository.findQuery(id, name,code , population, pr);         	
		return new PaginetedResponse<State>(page,result.getTotalElements(),result.getContent());
	}

	@Override
	public State findById(Long id) throws NotFoundException, BadRequestException {
		Optional<State> stateExist = stateRepository.findById(id);
		return stateExist.orElseThrow(() -> new NotFoundException(String.format("Stato con id %s non trovato", id)));
	}

	@Override
	public State save(State state) throws BadRequestException, ConflictException {
		state.validateState();
		
		State stateExist = stateRepository.findFirstByCodeAndIdIsNot(state.getCode(), Optional.ofNullable(state.getId()).orElse(0L));
		if(stateExist != null) 
			throw new ConflictException(String.format("Stato con codice %s è già esistente", state.getCode()));
		return stateRepository.save(state);
	}

	@Override
	@Transactional
	public State update(Long id, State state) throws NotFoundException, BadRequestException, ConflictException {
		
		State stateExist = findById(id);
		
		if(stateExist != null) {

			if(!stateExist.getCode().equals(state.getCode())) {
				
				stateExist = stateRepository.findFirstByCodeAndIdIsNot(state.getCode(), Optional.ofNullable(id).orElse(0L));
				
				if(stateExist != null) 
					throw new ConflictException(String.format("Lo stato con codice %s è già esistente", state.getCode()));
			}	
			if(stateExist.getPopulation()>state.getPopulation())
			throw new BadRequestException(String.format("La popolazione puo essere solo incrementata", state.getPopulation()));
			
			state.setId(id);
			return save(state);
		} else {
			throw new NotFoundException(String.format("Stato con id %s non trovato", id));
		}
		
		
	}

	@Override
	public void delete(Long id) throws NotFoundException {
		State stateExist = stateRepository.findFirstById(id);
		if(stateExist == null) {
			throw new NotFoundException(String.format("Lo stato con id %s non esiste", id));
		}
		
		stateRepository.deleteById(id);
	}

	@Override
	public List<State> insertAll(List<State> uni) throws BadRequestException, ConflictException {
		return (List <State>) stateRepository.saveAll(uni);
	}





}
package Esercizio1.example.services;
import java.util.List;

import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.State;
import Esercizio1.example.classes.University;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;



public interface UniversityService {
	public PaginetedResponse<University>findList(Long id, String name,Long numeroIscritti,String code, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException;
	public University findById(Long id) throws NotFoundException, BadRequestException;
	public University save(University uni) throws BadRequestException, ConflictException, NotFoundException;
	public University update(Long id, University university) throws NotFoundException, BadRequestException, ConflictException;
	public void delete(Long id) throws NotFoundException;
	public List<University> insertAll(List<University> uni) throws BadRequestException, ConflictException, NotFoundException;
}
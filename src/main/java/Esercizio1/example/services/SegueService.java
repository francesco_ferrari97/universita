package Esercizio1.example.services;


import java.util.List;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.Segue;


public interface SegueService {
	
	public PaginetedResponse<Segue> findList(Long idCorso, Long idStudent, Integer page, Integer limit, String sort) throws Exception ;
	Segue save(Segue s) throws Exception;
	void delete(Long id) throws Exception;
	Segue findByIdCorsoAndIdStudent(Long idCorso, Long idStudent) throws Exception;
	List<Segue> insertAll(List<Segue> associazioni) throws Exception;
	Segue findByIdSegue(Long id) throws Exception;
}

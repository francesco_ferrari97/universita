package Esercizio1.example.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import Esercizio1.example.classes.Corso;
import Esercizio1.example.classes.Materia;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.repositories.MateriaRepository;
import Esercizio1.example.utils.Utilities;

@Service
public class MateriaServiceImpl implements MateriaService {
	
	private final MateriaRepository materiaRepository;

	public MateriaServiceImpl(MateriaRepository materiaRepository) {
		this.materiaRepository = materiaRepository;
	}

	@Value("${Esercizio1.default-limit}")
	private Integer defaultLimit;
	@Value("${Esercizio1.max-limit}")
	private Integer maxLimit;

	@Override
	public PaginetedResponse<Materia> findList(Long id, String nomeMateria, String codiceMateria, String materiaPropedeutica,Integer cfu, Integer page,Integer limit,String sort) throws BadRequestException, NotFoundException {

		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		page = Optional.ofNullable(page).orElse(0);
		//code = Optional.ofNullable(code).map(c-> "%" + c + "%").orElse(null);
		if(limit > maxLimit)
			throw new BadRequestException(String.format("Il valore massimo per limit è : %s", maxLimit));
		
		if(nomeMateria != null) {
			Materia materiaExist= materiaRepository.findFirstByNomeMateria(nomeMateria);
			if(materiaExist == null) {
				throw new NotFoundException(String.format("Materia con nome %s non trovata.", nomeMateria));
				}
			}
		
		if(codiceMateria != null) {
			Materia materiaExist= materiaRepository.findFirstByCodiceMateria(codiceMateria);
			if(materiaExist == null) {
				throw new NotFoundException(String.format("Materia con codice %s non trovata.", codiceMateria));
				}
			}
		
		if(materiaPropedeutica != null) {
			Materia materiaExist= materiaRepository.findFirstByMateriaPropedeutica(materiaPropedeutica);
			if(materiaExist == null) {
				throw new NotFoundException(String.format("Materia con nome %s non trovata.", materiaPropedeutica));
				}
			}
		
		if(cfu != null) {
			Materia materiaExist= materiaRepository.findFirstByCfu(cfu);
			if(materiaExist == null) {
				throw new NotFoundException(String.format("Materia con cfu %s non trovata.", cfu));
				}
			}
		
		

		Sort s = Utilities.buildingSort(sort);
		PageRequest pr = null;
		if(s!=null) {
			pr=PageRequest.of(page, limit, s);
		} else {
			pr=PageRequest.of(page, limit);
		}

		Page<Materia>result=materiaRepository.findQuery(id,nomeMateria,codiceMateria,materiaPropedeutica,cfu,pr);         	
		return new PaginetedResponse<Materia>(page,result.getTotalElements(),result.getContent());
	}

	@Override
	public Materia findById(Long id) throws NotFoundException, BadRequestException {
		Optional<Materia> materiaExist = materiaRepository.findById(id);
		return materiaExist.orElseThrow(() -> new NotFoundException(String.format("Materia con id %s non trovata", id)));
	}

	@Override
	public Materia save(Materia materia) throws BadRequestException, ConflictException {
		materia.validateMateria();
		Materia materiaExist = materiaRepository.findFirstByCodiceMateriaAndIdIsNot(materia.getCodiceMateria(), Optional.ofNullable(materia.getId()).orElse(0L));

		if(materiaExist != null) 
			throw new ConflictException(String.format("Materia con codice %s è già esistente", materia.getCodiceMateria()));
		return materiaRepository.save(materia);
	}

	@Override
	@Transactional
	public Materia update(Long id, Materia materia) throws NotFoundException, BadRequestException, ConflictException {
		
		Materia materiaExist = findById(id);
	
		if(materiaExist != null) {

			if(!materiaExist.getCodiceMateria().equals(materia.getCodiceMateria())) {
				
				materiaExist = materiaRepository.findFirstByCodiceMateriaAndIdIsNot(materia.getCodiceMateria(), Optional.ofNullable(id).orElse(0L));
				
				if(materiaExist != null) 
					throw new ConflictException(String.format("Materia con codice %s è già esistente", materia.getCodiceMateria()));
			}	
			materia.setId(id);
			return save(materia);
		} else {
			throw new NotFoundException(String.format("Materia con id %s non trovata", id));
		}
	}

	@Override
	public void delete(Long id) throws NotFoundException {
		
		Materia materiaExist = materiaRepository.findFirstById(id);
		
		if(materiaExist == null) {
			throw new NotFoundException(String.format("Materia con id %s non esiste", id));
		} else {
			materiaRepository.deleteById(id);
		}
	}

	@Override
	public List<Materia> insertAll(List<Materia> materie) throws BadRequestException, ConflictException {
		return (List <Materia>) materiaRepository.saveAll(materie);
	}

}

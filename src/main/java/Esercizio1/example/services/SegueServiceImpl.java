package Esercizio1.example.services;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import Esercizio1.example.classes.Corso;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.Segue;
import Esercizio1.example.classes.Student;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.repositories.CorsoRepository;
import Esercizio1.example.repositories.SegueRepository;
import Esercizio1.example.repositories.StudentRepository;
import Esercizio1.example.utils.Utilities;

@Service
public class SegueServiceImpl implements SegueService{
	
	
	private final SegueRepository segueRepository;
	private final StudentRepository studentRepository;
	private final CorsoRepository corsoRepository;
	
	public SegueServiceImpl(SegueRepository segueRepository, StudentRepository studentRepository, CorsoRepository corsoRepository) {
		this.segueRepository = segueRepository;
		this.studentRepository = studentRepository;
		this.corsoRepository = corsoRepository;
	}
	
	@Value("${Esercizio1.default-limit}")
	private Integer defaultLimit;
	@Value("${Esercizio1.max-limit}")
	private Integer maxLimit;
	
	@Override
	public Segue findByIdSegue(Long id) throws NotFoundException, BadRequestException {
		Optional<Segue> segueExist = segueRepository.findById(id);//.map(cor -> {cor.materieStringToList(); return cor;});
		return segueExist.orElseThrow(() -> new NotFoundException(String.format("Associazione con id %s non trovato", id)));
	}
	
	
	@Override
	public PaginetedResponse<Segue> findList(Long idCorso, Long idStudent, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException {
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		page = Optional.ofNullable(page).orElse(0);
		
		if(idStudent!=null) {
			Segue segueExist = segueRepository.findFirstByIdStudent(idStudent);
			if(segueExist == null) {
				throw new NotFoundException(String.format("Associazione con id della studente %s non trovata.", idStudent));
			}
		}
		
		if(idCorso!=null) {
			Segue segueExist = segueRepository.findFirstByIdCorso(idCorso);
			if(segueExist == null) {
				throw new NotFoundException(String.format("Associazione con id del corso %s non trovata.", idCorso));
			}
		}
		
		if(limit > maxLimit)
			throw new BadRequestException(String.format("Il valore massimo per limit è : %s", maxLimit));

		Sort s = Utilities.buildingSort(sort);
		PageRequest pr = null;
		if(s!=null) {
			pr=PageRequest.of(page, limit, s);
		} else {
			pr=PageRequest.of(page, limit);
		}
		
		Page<Segue>result=segueRepository.findQuery(idStudent, idCorso,pr);
		return new PaginetedResponse<Segue>(page,result.getTotalElements(),result.getContent());
	}
	
	
	
	
	@Override
	public Segue findByIdCorsoAndIdStudent(Long idCorso, Long idStudent) throws NotFoundException, BadRequestException {
		
		Student studentExist = studentRepository.findFirstById(idStudent);
		Corso corsoExist = corsoRepository.findFirstById(idCorso);
		
		if(studentExist == null) throw new NotFoundException("Lo studente non esiste");
		if(corsoExist == null) throw new NotFoundException("Il corso non esiste");
		
		return segueRepository.findByIdCorsoAndIdStudent(idCorso, idStudent);
	}

	

	@Override
	public Segue save(Segue s) throws NotFoundException, BadRequestException, ConflictException {
		
		Segue segueExist = segueRepository.findByIdCorsoAndIdStudent(s.getIdCorso(), s.getIdStudent());
		if(segueExist != null) throw new ConflictException("L'associazione è esistente");
		
		Student studentExist = studentRepository.findFirstById(s.getIdStudent());
		Corso corsoExist = corsoRepository.findFirstById(s.getIdCorso());
		
		if(studentExist == null) throw new NotFoundException("Lo studente non esiste");
		if(corsoExist == null) throw new NotFoundException("Il corso non esiste");
		
		if(!studentExist.getUniversity().getName().equals(corsoExist.getUniversity().getName())) {
			throw new BadRequestException("Lo studente è iscritto a un'altra università.");
		}
		
		
		
		if(!studentExist.getCorso().isEmpty()) {
			for(Corso corso : studentExist.getCorso()) {
				if(corso.getAmbito()==corsoExist.getAmbito()) {
					throw new BadRequestException("Stesso ambito");
				}
			}
		}
		
		return segueRepository.save(s);
	}

	
	@Override
	public void delete(Long id) throws NotFoundException {	
		Segue segueExist = segueRepository.findFirstById(id);
		Optional.ofNullable(segueExist).orElseThrow(()-> new NotFoundException(String.format("L'associazione con id %s non esiste.", id)));
		segueRepository.deleteById(id);
		
	}

	@Transactional
	@Override
	public List<Segue> insertAll(List<Segue> associazioni) throws Exception {
		
		for(Segue a : associazioni) {

			Student stu = studentRepository.findFirstById(a.getIdStudent());
			Optional.ofNullable(stu).orElseThrow(()-> new NotFoundException(String.format("Lo studente con id %s non esiste.",a.getIdStudent())));
			
			Corso cor = corsoRepository.findFirstById(a.getIdCorso());
			Optional.ofNullable(cor).orElseThrow(()-> new NotFoundException(String.format("Il corso con id %s non esiste", a.getIdCorso())));
			

			a.setIdCorso(cor.getId());
			a.setIdStudent(stu.getId());
		}

		return (List<Segue>) segueRepository.saveAll(associazioni);
	}


	




	
}

package Esercizio1.example.services;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import Esercizio1.example.classes.Corso;
import Esercizio1.example.classes.Libretto;
import Esercizio1.example.classes.Materia;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.Student;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.repositories.CorsoRepository;
import Esercizio1.example.repositories.LibrettoRepository;
import Esercizio1.example.repositories.MateriaRepository;
import Esercizio1.example.repositories.StudentRepository;
import Esercizio1.example.utils.Utilities;

@Service
public class LibrettoServiceImpl implements LibrettoService {

	private final LibrettoRepository librettoRepository;
	private final StudentRepository studentRepository;
	private final MateriaRepository materiaRepository;
	private final CorsoRepository corsoRepository;

	@Autowired
	public LibrettoServiceImpl(LibrettoRepository librettoRepository, StudentRepository studentRepository, MateriaRepository materiaRepository, CorsoRepository corsoRepository) {
		this.librettoRepository = librettoRepository;
		this.studentRepository = studentRepository;
		this.materiaRepository = materiaRepository;
		this.corsoRepository = corsoRepository;
	}

	@Value("${Esercizio1.default-limit}")
	private Integer defaultLimit;
	@Value("${Esercizio1.max-limit}")
	private Integer maxLimit;

	@Override
	public PaginetedResponse<Libretto> findList(Long id, String matricola, String codiceMateria, String nomeCorso,LocalDate dataSostenimento, Integer voto, Integer page, Integer limit, String sort)throws BadRequestException, NotFoundException {
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		page = Optional.ofNullable(page).orElse(0);

		if(limit > maxLimit)
			throw new BadRequestException(String.format("Il valore massimo per limit è : %s", maxLimit));
		
		if(matricola!=null) {
			Student studExist = studentRepository.findFirstByMatricola(matricola);
			if(studExist == null) {
				throw new NotFoundException(String.format("Studente con matricola %s non trovato.", matricola));
			}
		}
		
		if(codiceMateria!=null) {
			Materia materiaExist = materiaRepository.findFirstByCodiceMateria(codiceMateria);
			if(materiaExist == null) {
				throw new NotFoundException(String.format("Materia con codice %s non trovata.", codiceMateria));
			}
		}
		
		if(nomeCorso!=null) {
			Corso corsoExist = corsoRepository.findFirstByNome(nomeCorso);
			if(corsoExist == null) {
				throw new NotFoundException(String.format("Corso con nome %s non trovato.", nomeCorso));
			}
		}
		
		if(dataSostenimento!=null) {
			Libretto dataExist = librettoRepository.findFirstByData(dataSostenimento);
			if(dataExist == null) {
				throw new NotFoundException(String.format("Il record con data %s non trovato.", dataSostenimento));
			}
		}
		
		if(voto!=null) {
			Libretto librettoExist = librettoRepository.findFirstByVoto(voto);
			if(librettoExist == null) {
				throw new NotFoundException(String.format("Il record con voto %s non trovato.", voto));
			}
		}

		Sort s = Utilities.buildingSort(sort);

		PageRequest pr = null;

		if(s!=null) {
			pr=PageRequest.of(page, limit, s);
		} else {
			pr=PageRequest.of(page, limit);
		}

		Page<Libretto>result=librettoRepository.findQuery(id, matricola, codiceMateria, nomeCorso, dataSostenimento, voto, pr);
		return new PaginetedResponse<Libretto>(page,result.getTotalElements(),result.getContent());
	}

	/*Metodo salva
	 * 1) Valido il libretto 
	 * 2) Assegno a una variabile di tipo Libretto l'esame quindi tramite gli attributi del libretto che sto inserendo cerco l'id di Studente,Corso e Materia.
	 * 3) Se non ho trovato l'esame significa che non l'ho ancora dato.
	 * 4) Mi cerco la materia propedeutica quindi la prima che mi permette l'accesso alla seconda.
	 * 5) Se la trovo allora mi salvo l'esame assegnando la materia annessa allo studente e al corso.
	 * 6) Se non ho la materia propedeutica allora lancio l'errore.
	 * 7) Se l'esame esiste lo trovo e mi prendo il voto e controllo se è maggiore di 17. Se il voto è maggiore significa che l'ho già fatto e passato.
	 * 8) Infine salvo il record con tutti i dati.
	 */
	@Override
	public Libretto save(Libretto l) throws BadRequestException, NotFoundException, ConflictException {

		/*1)*/	l.validateLibretto();
		
		/*2)*/  Libretto recordExist = librettoRepository.findExam(l.getStudent().getId(), l.getCorso().getId(), l.getMateria().getId());
		
		
		
		Student studentExist = studentRepository.findFirstByMatricola(l.getStudent().getMatricola());
		if(studentExist == null) throw new NotFoundException("Lo studente non esiste");
		l.setStudent(studentExist);
		
		Materia materiaExist = materiaRepository.findFirstByCodiceMateria(l.getMateria().getCodiceMateria());
		if(materiaExist == null) throw new NotFoundException("La materia non esiste");
		l.setMateria(materiaExist);
		
		Corso corsoExist = corsoRepository.findFirstByNome(l.getCorso().getNome());
		if(corsoExist == null) throw new NotFoundException("Il corso non esiste");
		l.setCorso(corsoExist);
		
		
		/*if(!l.getStudent().getUniversity().getName().equalsIgnoreCase(l.getCorso().getUniversity().getName())) {
			throw new BadRequestException("Lo studente non è iscritto all'università con quel corso");
		}
		
		if(!l.getStudent().getCorso().contains(corsoExist)) {
			throw new BadRequestException("Lo studente non frequenta questo corso");
		}
		
		if(!l.getCorso().getMaterie().contains(materiaExist)) {
			throw new BadRequestException("Il corso non ha questa materia");
		}
		*/
		
		/*3)*/  if(recordExist == null) {

			/*4)*/    String propedeutica = materiaRepository.findFirstById(l.getMateria().getId()).getMateriaPropedeutica();

			/*5)*/	     if(propedeutica != null) {
				         recordExist = librettoRepository.findExamPassed(propedeutica, l.getStudent().getId(), l.getCorso().getId());

				/*6)*/	         if(recordExist == null) throw new ConflictException("Non puoi dare questo esame perchè non hai la propedeuticità della materia");
			} 		
		} else {
			/*7)*/ if(recordExist.getVoto() > 17) {
				throw new ConflictException(String.format("Lo studente %s ha già dato la materia %s con voto %s", recordExist.getStudent().getMatricola(), recordExist.getMateria().getNomeMateria(),recordExist.getVoto()));
			}
		}
		/*8)*/ return librettoRepository.save(l);
	}

	@Override
	public Libretto update(Long id, Libretto l) throws NotFoundException, BadRequestException, ConflictException {

		Libretto recordExist = findById(id);

		if(recordExist != null) {			
				l.setId(id);
				return save(l);
		} else {
			throw new NotFoundException(String.format("Record con id %s non trovato", id));
		}
	}

	@Override
	public void delete(Long id) throws NotFoundException {
		
		Libretto recordExist = librettoRepository.findFirstById(id);
		if(recordExist == null) {
			throw new NotFoundException(String.format("Record con id %s non esiste", id));
		} else {
			librettoRepository.deleteById(id);
		}
	}

	@Override
	public Libretto findById(Long id) throws NotFoundException{
		Optional<Libretto> lib = librettoRepository.findById(id);
		return lib.orElseThrow(() -> new NotFoundException(String.format("Record con id %s non trovato", id)));
	}


	@Override
	public List<Libretto> insertAll(List<Libretto> record) throws NotFoundException {
		
		for(Libretto l : record) {
		
	    Student stu = studentRepository.findFirstByMatricola(l.getStudent().getMatricola());
		Materia mat = materiaRepository.findFirstByCodiceMateria(l.getMateria().getCodiceMateria());
		Corso cor = corsoRepository.findFirstByNome(l.getCorso().getNome());
		
		if(stu==null) {
			throw new NotFoundException(String.format("Lo studente con matricola %s non esiste.", l.getStudent().getMatricola()));
		}
		
		if(mat==null) {
			throw new NotFoundException(String.format("La materia con codice %s non esiste", l.getMateria().getCodiceMateria()));
		}
		
		if(cor==null) {
			throw new NotFoundException(String.format("Il corso con nome %s non esiste", l.getCorso().getNome()));
		}
		
		l.setCorso(cor);
		l.setMateria(mat);
		l.setStudent(stu);
		}
		
		return (List<Libretto>) librettoRepository.saveAll(record);
		
	}









}

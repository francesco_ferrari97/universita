package Esercizio1.example.services;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.State;
import Esercizio1.example.classes.Student;
import Esercizio1.example.classes.University;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.repositories.StateRepository;
import Esercizio1.example.repositories.UniversityRepository;
import Esercizio1.example.utils.Utilities;

@Service
public class UniversityServiceImpl implements UniversityService {

	private final UniversityRepository universityRepository;
	private final StateRepository stateRepository;
	
	public UniversityServiceImpl(UniversityRepository universityRepository, StateRepository stateRepository) {
		this.universityRepository = universityRepository;
		this.stateRepository = stateRepository;
	}
	
	@Value("${Esercizio1.max-iscritti}")
	private Long maxIscritti;
	@Value("${Esercizio1.default-limit}")
	private Integer defaultLimit;
	@Value("${Esercizio1.max-limit}")
	private Integer maxLimit;
	

	@Override
	public PaginetedResponse<University> findList(Long id, String name, Long numeroIscritti,String code, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException {
		
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		page = Optional.ofNullable(page).orElse(0);

		if(limit > maxLimit)
		throw new BadRequestException(String.format("Il valore massimo per limit è : %s", maxLimit));
		
		if(name != null) {
		University uniExist= universityRepository.findFirstByName(name);
		if(uniExist == null) {
			throw new NotFoundException(String.format("Universita con nome %s non trovato.", name));
			}
		}
		
		if(numeroIscritti != null) {
			University uniExist= universityRepository.findFirstByNumeroIscritti(numeroIscritti);
			if(uniExist == null) {
				throw new NotFoundException(String.format("Universita con numero d'iscritti %s non trovato.", numeroIscritti));
				}
		}
		
		if(code != null) {
		State uniExist= stateRepository.findFirstByCode(code);
			if(uniExist == null) {
				throw new NotFoundException(String.format("Universita con codice stato %s non trovato.", code));
				}
		}
		
		
		
		
		Sort s = Utilities.buildingSort(sort);
		
		PageRequest pr = null;
		
		if(s!=null) {
			pr=PageRequest.of(page, limit, s);
		} else {
			pr=PageRequest.of(page, limit);
		}
		
		Page<University>result=universityRepository.findQuery(id, name, numeroIscritti,code, pr);
		return new PaginetedResponse<University>(page,result.getTotalElements(),result.getContent());
	}

	@Override
	public University findById(Long id) throws NotFoundException, BadRequestException {
		Optional<University> uniExist = universityRepository.findById(id);
		return uniExist.orElseThrow(() -> new NotFoundException(String.format("Università con id %s non trovato", id)));
	}
	
	@Override
	public University save(University uni) throws BadRequestException, ConflictException, NotFoundException {
		
		uni.validateUniversity(maxIscritti);
		University uniExist = universityRepository.findFirstByNameAndIdIsNot(uni.getName(), Optional.ofNullable(uni.getId()).orElse(0L));
		if(uniExist != null)throw new ConflictException(String.format("L'università con nome %s è già esistente", uni.getName()));
		
		State s = stateRepository.findFirstByCode(uni.getState().getCode());
		if(s == null) throw new NotFoundException(String.format("Stato con id %s non esiste", uni.getState().getCode()));
		
		uni.setState(s);
		return universityRepository.save(uni);
	}
	
	@Override
	@Transactional
	public University update(Long id, University uni) throws NotFoundException, BadRequestException, ConflictException {
		
		University uniExist = findById(id);
		State s = stateRepository.findFirstByCode(uni.getState().getCode());
		
		if (uniExist != null) {
			if(!uniExist.getName().equals(uni.getName())){
				uniExist = universityRepository.findFirstByNameAndIdIsNot(uni.getName(), Optional.ofNullable(id).orElse(0L));
				if(uniExist != null) throw new ConflictException(String.format("L'università con nome %s è già esistente", uni.getName()));
			}
			if(s == null) throw new NotFoundException("Lo stato non esiste");
			uni.setState(s);
			uni.setId(id);
			return save(uni);
		} else {
			throw new NotFoundException(String.format("Università con id %s non trovata", id));
		}
	}
	
	@Override
	public void delete(Long id) throws NotFoundException {
		University uni = universityRepository.findFirstById(id);
		
		if (uni == null) {
			throw new NotFoundException(String.format("L'università con id %s non esiste.", id));
		}
			universityRepository.deleteById(id);
		
	}


	@Override
	public List<University> insertAll(List<University> uni) throws BadRequestException, ConflictException, NotFoundException {
		
		for(University u : uni) {
			State uniExist = stateRepository.findFirstByCode(u.getState().getCode());
			
			if(uniExist==null) {
				throw new NotFoundException(String.format("Lo stato con id %s non esiste.", u.getState().getId()));
			}
			
			u.setState(uniExist);
			}
			
			return (List<University>) universityRepository.saveAll(uni);
		
	}
	
}
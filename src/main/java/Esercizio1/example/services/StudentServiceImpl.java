package Esercizio1.example.services;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.Student;
import Esercizio1.example.classes.University;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import Esercizio1.example.repositories.StudentRepository;
import Esercizio1.example.repositories.UniversityRepository;
import Esercizio1.example.utils.Utilities;



@Service
public class StudentServiceImpl implements StudentService{

	private final StudentRepository studentRepository;
	private final UniversityRepository universityRepository;
	
	@Value("${Esercizio1.default-limit}")
	private Integer defaultLimit;
	@Value("${Esercizio1.max-limit}")
	private Integer maxLimit;

	@Autowired
	public StudentServiceImpl(StudentRepository studentRepository, UniversityRepository universityRepository) {
		this.studentRepository = studentRepository;
		this.universityRepository = universityRepository;
	}

	@Override
	public PaginetedResponse<Student> findList(Long id, String name, String lastName, String matricola, LocalDate dataNascita, LocalDate dataIscrizione, Integer page,Integer limit,String sort) throws BadRequestException, NotFoundException {		
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		page = Optional.ofNullable(page).orElse(0);
		matricola = Optional.ofNullable(matricola).map(m -> "%" + m + "%").orElse(null);
		
		if(matricola!=null) {
			Student studExist = studentRepository.findFirstByMatricola(matricola);
			if(studExist == null) {
				throw new NotFoundException(String.format("Studente con matricola %s non trovato.", studExist));
			}
		}
		
		if(name!=null) {
			Student studExist = studentRepository.findFirstByName(name);
			if(studExist == null) {
				throw new NotFoundException(String.format("Studente con nome %s non trovato.", studExist));
			}
		}
		
		if(lastName!=null) {
			Student studExist = studentRepository.findFirstByLastName(lastName);
			if(studExist == null) {
				throw new NotFoundException(String.format("Studente con cognome %s non trovato.", studExist));
			}
		}
		
		if(dataNascita!=null) {
			Student studExist = studentRepository.findFirstByDataNascita(dataNascita);
			if(studExist == null) {
				throw new NotFoundException(String.format("Studente con data di nascita %s non trovato.", studExist));
			}
		}
		
		if(dataIscrizione!=null) {
			Student studExist = studentRepository.findFirstByDataIscrizione(dataIscrizione);
			if(studExist == null) {
				throw new NotFoundException(String.format("Studente con data d'iscrizione %s non trovato.", studExist));
			}
		}
		
		
		if(limit > maxLimit)
		throw new BadRequestException(String.format("Il valore massimo per limit è : %s", maxLimit));
		Sort s = Utilities.buildingSort(sort);
		PageRequest pr = null;
		if(s!=null) {
			pr=PageRequest.of(page, limit, s);
		} else {
			pr=PageRequest.of(page, limit);
		}
		Page<Student>result=studentRepository.findQuery(id, name, lastName, matricola, dataNascita, dataIscrizione, pr);         	
		return new PaginetedResponse<Student>(page,result.getTotalElements(),result.getContent());
	}

	@Override
	public Student findById(Long id) throws NotFoundException{
		Student stud = studentRepository.findFirstById(id);
		if(stud == null) {
			throw new NotFoundException(String.format("Studente con id %s non trovato", id));
		}
		return stud;
	}
	
	@Override
	public Student save(Student student) throws BadRequestException, ConflictException, NotFoundException {
		
		student.validateStudent();
		
		Student studentExist = studentRepository.findFirstByMatricolaAndIdIsNot(student.getMatricola(), Optional.ofNullable(student.getId()).orElse(0L));
		if(studentExist != null) throw new ConflictException(String.format("Lo studente con la matricola %s è già esistente", student.getMatricola()));
		
		University uni = universityRepository.findFirstByName(student.getUniversity().getName());
		
		if(uni==null) {
			throw new NotFoundException(String.format("L'università con id %s non esiste.", student.getUniversity().getId()));
		}
		
		student.setUniversity(uni);
		
		return studentRepository.save(student);
	}
	

	@Override
	@Transactional
	public Student update(Long id, Student student) throws NotFoundException, BadRequestException, ConflictException {
	
	Student studentExist = findById(id);
	University uni = universityRepository.findFirstByName(student.getUniversity().getName());
	
		if (studentExist != null) {
			
			if(!studentExist.getMatricola().equals(student.getMatricola())){
				studentExist = studentRepository.findFirstByMatricolaAndIdIsNot(student.getMatricola(), Optional.ofNullable(id).orElse(0L));
				if(studentExist != null) {
					 throw new ConflictException(String.format("Studente con matricola %s è già esistente", student.getMatricola()));
				} 	
			
				if(uni==null) throw new NotFoundException("L'università non esiste");
				student.setUniversity(uni);				
			}
			student.setId(id);
			return save(student);
		} else {
			throw new NotFoundException(String.format("Studente con id %s non trovato", id));
		}
	}
	
	
	@Override
	public void delete(Long id) throws NotFoundException {
		Student studentExist = studentRepository.findFirstById(id);
		if(studentExist == null) {
			throw new NotFoundException(String.format("Lo studente con id %s non esiste", id));
		}
		
		studentRepository.deleteById(id);
	}

	@Override
	public List<Student> insertAll(List<Student> students) throws NotFoundException, ConflictException, BadRequestException {
		
		for(Student s : students) {
			
			s.validateStudent();
			
			Student studentExist = studentRepository.findFirstByMatricolaAndIdIsNot(s.getMatricola(), Optional.ofNullable(s.getId()).orElse(0L));
			if(studentExist != null) throw new ConflictException(String.format("Lo studente con la matricola %s è già esistente", s.getMatricola()));
			
			University uni = universityRepository.findFirstByName(s.getUniversity().getName());
			
			if(uni==null) {
				throw new NotFoundException(String.format("L'università con id %s non esiste.", s.getUniversity().getId()));
			}
			
			s.setUniversity(uni);
		}
		
		return (List<Student>) studentRepository.saveAll(students);
		
	}
}

package Esercizio1.example.services;

import java.util.List;

import Esercizio1.example.classes.Corso;
import Esercizio1.example.classes.Materia;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.University;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;

public interface CorsoService {

	PaginetedResponse<Corso> findList(Long id, String nome, String ambito,Integer durata, University university, Integer page, Integer limit, String sort)throws BadRequestException, NotFoundException;
	Corso findById(Long id) throws NotFoundException, BadRequestException ;
	Corso save(Corso c) throws BadRequestException, ConflictException, NotFoundException;
	Corso update(Long id, Corso corso) throws NotFoundException, BadRequestException, ConflictException;
	void delete(Long id) throws NotFoundException;
	List<Corso> insertAll(List<Corso> corsi) throws BadRequestException, ConflictException, NotFoundException;
	
}

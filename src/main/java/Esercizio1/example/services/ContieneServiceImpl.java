package Esercizio1.example.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import Esercizio1.example.classes.Contiene;
import Esercizio1.example.classes.Corso;
import Esercizio1.example.classes.Libretto;
import Esercizio1.example.classes.Materia;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.Student;
import Esercizio1.example.classes.University;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.repositories.ContieneRepository;
import Esercizio1.example.repositories.CorsoRepository;
import Esercizio1.example.repositories.MateriaRepository;
import Esercizio1.example.utils.Utilities;

@Service
public class ContieneServiceImpl implements ContieneService {

	private final ContieneRepository contieneRepository;
	private final CorsoRepository corsoRepository;
	private final MateriaRepository materiaRepository;
	
	@Value("${Esercizio1.default-limit}")
	private Integer defaultLimit;
	@Value("${Esercizio1.max-limit}")
	private Integer maxLimit;
	
	
	@Autowired
	public ContieneServiceImpl(ContieneRepository contieneRepository, CorsoRepository corsoRepository, MateriaRepository materiaRepository) {
		this.contieneRepository = contieneRepository;
		this.corsoRepository = corsoRepository;
		this.materiaRepository = materiaRepository;
		
	}
	
	@Override
	public Contiene findByIdContiene(Long id) throws NotFoundException, BadRequestException {
		Optional<Contiene> contieneExist = contieneRepository.findById(id);//.map(cor -> {cor.materieStringToList(); return cor;});
		return contieneExist.orElseThrow(() -> new NotFoundException(String.format("Associazione con id %s non trovato", id)));
	}
	

	@Override
	public PaginetedResponse<Contiene> findListContains(Long idMateria, Long idCorso, Integer page, Integer limit,String sort) throws BadRequestException, NotFoundException {
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		page = Optional.ofNullable(page).orElse(0);
		
		if(idMateria!=null) {
			Contiene contieneExist = contieneRepository.findFirstByIdMateria(idMateria);
			if(contieneExist == null) {
				throw new NotFoundException(String.format("Associazione con id della materia %s non trovata.", idMateria));
			}
		}
		
		if(idCorso!=null) {
			Contiene contieneExist = contieneRepository.findFirstByIdCorso(idCorso);
			if(contieneExist == null) {
				throw new NotFoundException(String.format("Associazione con id del corso %s non trovata.", idCorso));
			}
		}
		
		if(limit > maxLimit)
			throw new BadRequestException(String.format("Il valore massimo per limit è : %s", maxLimit));

		Sort s = Utilities.buildingSort(sort);
		PageRequest pr = null;
		if(s!=null) {
			pr=PageRequest.of(page, limit, s);
		} else {
			pr=PageRequest.of(page, limit);
		}
		
		Page<Contiene>result=contieneRepository.findQuery(idMateria, idCorso, pr);
		return new PaginetedResponse<Contiene>(page,result.getTotalElements(),result.getContent());
	}
	
	@Override
	public Contiene findByIdCorsoAndIdMateria(Long idCorso, Long idMateria) throws NotFoundException {
		
		Materia materiaExist =  materiaRepository.findFirstById(idMateria);
		Corso corsoExist = corsoRepository.findFirstById(idCorso);
		
		if(materiaExist == null) throw new NotFoundException("Materia non esistente");
		if(corsoExist == null) throw new NotFoundException("Corso non esistente");
		
		return contieneRepository.findByIdCorsoAndIdMateria(idCorso, idMateria);
		
	}


	@Override
	public void delete(Long id) throws NotFoundException{
		
		Contiene containsExist = contieneRepository.findFirstById(id);
		
		if(containsExist == null) {
			throw new NotFoundException(String.format("Associazione con id %s non esiste", id));
		} else {
			contieneRepository.deleteById(id);
		}
		
	}
	
	
	

	@Override
	public Contiene save(Contiene c) throws NotFoundException, BadRequestException, ConflictException {
		
		Materia materiaExist = materiaRepository.findFirstById(c.getIdMateria());
		Corso corsoExist = corsoRepository.findFirstById(c.getIdCorso());
		
		Contiene containsExist = contieneRepository.findByIdCorsoAndIdMateria(c.getIdCorso(), c.getIdMateria());
		
		if(containsExist != null) throw new ConflictException("L'associazione è esistente");
		
		if(materiaExist == null) throw new NotFoundException("La materia non esiste");
		if(corsoExist == null) throw new NotFoundException("Il corso non esiste");
		
		
		return contieneRepository.save(c);
	}
	
	@Transactional
	@Override
	public List<Contiene> insertAll(List<Contiene> associazioni) throws NotFoundException {
		
		for(Contiene a : associazioni) {
		
		Materia mat = materiaRepository.findFirstById(a.getIdMateria());
		Corso cor = corsoRepository.findFirstById(a.getIdCorso());
		
		if(mat==null) {
			throw new NotFoundException(String.format("La materia con id %s non esiste", a.getIdMateria()));
		}
		
		if(cor==null) {
			throw new NotFoundException(String.format("Il corso con id %s non esiste", a.getIdCorso()));
		}
		
		a.setIdCorso(cor.getId());
		a.setIdMateria(mat.getId());
		}
		
		return (List<Contiene>) contieneRepository.saveAll(associazioni);
		
	}




}

package Esercizio1.example.services;

import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.State;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;

import java.util.List;


public interface StateService {
	PaginetedResponse<State> findList(Long id, String name, String code, Long population, Integer page,Integer limit, String sort) throws BadRequestException, NotFoundException;
    State findById(Long id) throws NotFoundException, BadRequestException ;
    State save(State s) throws BadRequestException, ConflictException;
	State update(Long id, State state) throws NotFoundException, BadRequestException, ConflictException;
	void delete(Long id) throws NotFoundException;
	List<State> insertAll(List<State> uni) throws BadRequestException, ConflictException;
}
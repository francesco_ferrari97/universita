package Esercizio1.example.services;

import java.time.LocalDate;
import java.util.List;
import Esercizio1.example.classes.Libretto;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;

public interface LibrettoService {
	
	List<Libretto>insertAll(List<Libretto>l) throws NotFoundException ;
	PaginetedResponse<Libretto> findList(Long id, String matricola, String codiceMateria,String nomeCorso, LocalDate dataSostenimento, Integer voto, Integer page, Integer limit, String sort)throws BadRequestException, NotFoundException;
	Libretto findById(Long id) throws NotFoundException, BadRequestException ;
	Libretto save(Libretto l) throws BadRequestException, ConflictException, NotFoundException;
	Libretto update(Long id, Libretto l) throws NotFoundException, BadRequestException, ConflictException;
	void delete(Long id) throws NotFoundException;


}

package Esercizio1.example.services;

import java.util.List;

import Esercizio1.example.classes.Materia;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;

public interface MateriaService {

	PaginetedResponse<Materia> findList(Long id, String nomeMateria, String codiceMateria, String materiaPropedeutica,Integer cfu, Integer page, Integer limit, String sort)throws BadRequestException, NotFoundException;
    Materia findById(Long id) throws NotFoundException, BadRequestException ;
    Materia save(Materia m) throws BadRequestException, ConflictException;
	Materia update(Long id, Materia materia) throws NotFoundException, BadRequestException, ConflictException;
	void delete(Long id) throws NotFoundException;
	List<Materia> insertAll(List<Materia> materie) throws BadRequestException, ConflictException;
}

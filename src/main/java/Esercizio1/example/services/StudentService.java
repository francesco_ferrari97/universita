package Esercizio1.example.services;
import Esercizio1.example.classes.*;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;

import java.time.LocalDate;
import java.util.List;

import org.aspectj.weaver.NewFieldTypeMunger;

public interface StudentService {
	List<Student>insertAll(List<Student>s) throws Exception ;
	PaginetedResponse<Student>findList(Long id, String name, String lastName, String matricola, LocalDate dataNascita, LocalDate dataIscrizione, Integer page,Integer limit,String sort) throws BadRequestException,NotFoundException;
	Student findById(Long id) throws NotFoundException;
	Student save(Student s) throws BadRequestException, ConflictException, NotFoundException;
	Student update(Long id, Student student) throws NotFoundException, BadRequestException, ConflictException;
	void delete(Long id) throws NotFoundException;
}

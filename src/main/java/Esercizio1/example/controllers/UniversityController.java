package Esercizio1.example.controllers;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.State;
import Esercizio1.example.classes.University;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.services.UniversityService;

@RestController
@RequestMapping("/api/universities")
public class UniversityController {

	private final UniversityService universityService;
	
	public UniversityController (UniversityService universityService) {
		this.universityService = universityService;
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public PaginetedResponse <University> list(@RequestParam (required = false)Long id,
			                                   @RequestParam (required = false)String name,
			                                   @RequestParam (required = false)Long numeroIscritti,
			                                   @RequestParam (required = false)String code, 
											   @RequestParam (required = false)Integer page,
											   @RequestParam (required = false)Integer limit,
											   @RequestParam (required=false) String sort) throws BadRequestException, NotFoundException {
		return universityService.findList(id,name,numeroIscritti,code,page,limit,sort);
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public University findUniversity(@PathVariable("id") Long id) throws NotFoundException, BadRequestException {
		return universityService.findById(id);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public University createUniversity(@RequestBody University uni) throws BadRequestException, ConflictException, NotFoundException {
		return universityService.save(uni);
	}
	
	 @PostMapping("/bulk-insert")
	    public List<University> createMoreUniversity(@RequestBody List<University> uni) throws BadRequestException, ConflictException, NotFoundException {
	        if(uni != null && !uni.isEmpty()) 
	        universityService.insertAll(uni);
	        return uni;
	    }
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public University updateUniversity(@PathVariable("id") Long id, @RequestBody University uni) throws NotFoundException, BadRequestException, ConflictException {
		return universityService.update(id, uni);
	}
	
	@Transactional
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUniversity(@PathVariable("id")Long id) throws NotFoundException {
		universityService.delete(id);
	}
	
	
}
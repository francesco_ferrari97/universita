package Esercizio1.example.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import Esercizio1.example.classes.Contiene;
import Esercizio1.example.classes.Corso;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.Segue;
import Esercizio1.example.classes.University;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.services.ContieneService;
import Esercizio1.example.services.CorsoService;
import Esercizio1.example.services.SegueService;


@RestController
@RequestMapping("/api/corsi")
public class CorsoController {
	
	private final SegueService segueService;
	private final ContieneService contieneService;
	private final CorsoService corsoService;

	@Autowired
	public CorsoController(CorsoService corsoService, SegueService segueService, ContieneService contieneService) {
		this.corsoService = corsoService;
		this.segueService = segueService;
		this.contieneService = contieneService;
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public PaginetedResponse <Corso> list (@RequestParam (required = false)Long id,
										   @RequestParam (required = false)String nome,
										   @RequestParam (required = false)String ambito,
										   @RequestParam (required = false)Integer durata,
										   @RequestParam (required = false)University university,
										   @RequestParam (required = false)Integer page,
										   @RequestParam (required = false)Integer limit,
										   @RequestParam (required=false) String sort) throws BadRequestException,NotFoundException {

		return corsoService.findList(id,nome,ambito,durata,university,page,limit,sort);

	}
	
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Corso findById(@PathVariable("id")Long id) throws NotFoundException, BadRequestException  {
		return corsoService.findById(id); 
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Corso createCorso(@RequestBody Corso c) throws BadRequestException, ConflictException, NotFoundException {
		return corsoService.save(c);
	}
	
	@PostMapping("/bulk-insert")
    public List<Corso> createMoreCorsi(@RequestBody List<Corso> c) throws BadRequestException, ConflictException, NotFoundException {
        if(c != null && !c.isEmpty()) 
        corsoService.insertAll(c);
        return c;
    }
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Corso updateCorso(@PathVariable("id") Long id, @RequestBody Corso c) throws NotFoundException, BadRequestException, ConflictException {
		return corsoService.update(id, c);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCorso(@PathVariable("id")Long id) throws NotFoundException {
		corsoService.delete(id);
	}
	
	
	
	
	
	
	// TABELLA SEGUE
	@GetMapping("/segue/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Contiene findByIdSegue(@PathVariable("id")Long id) throws NotFoundException, BadRequestException  {
		return contieneService.findByIdContiene(id); 
	}
	
	@GetMapping("/segue")
	@ResponseStatus(HttpStatus.OK)
	public PaginetedResponse<Segue> findList(@RequestParam (required = false)Long idCorso, 
											 @RequestParam (required = false)Long idStudent,
											 @RequestParam (required = false)Integer Page,
											 @RequestParam (required = false)Integer limit,
											 @RequestParam (required = false)String sort
											 ) throws Exception {
		return segueService.findList(idCorso, idStudent, limit, limit, sort);
	}
	

	@GetMapping("/{idCorso}/students/{idStudent}")
	@ResponseStatus(HttpStatus.OK)
	public Segue findByIdCorsoAndIdStudent(@PathVariable("idCorso") Long idCorso, @PathVariable("idStudent") Long idStudent) throws Exception {
		return segueService.findByIdCorsoAndIdStudent(idCorso, idStudent);
	}
	

	@PostMapping("/segue/insert")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Segue addInSegue(@RequestBody Segue s) throws Exception {
		return segueService.save(s);
	}

	@DeleteMapping("/segue/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeFromSegue(@PathVariable("id") Long id) throws Exception{
		segueService.delete(id);
	}
	
	@PostMapping("/segue/bulk-insert")
    public List<Segue> createMoreSegue(@RequestBody List<Segue> s) throws Exception {
        if(s != null && !s.isEmpty()) 
        segueService.insertAll(s);
        return s;
    }
	
	
	
	
	
	// TABELLA CONTIENE
	@GetMapping("/contains/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Contiene findByIdContiene(@PathVariable("id")Long id) throws NotFoundException, BadRequestException  {
		return contieneService.findByIdContiene(id); 
	}
	
	@GetMapping("/{idCorso}/contiene/{idMateria}")
	@ResponseStatus(HttpStatus.OK)
	public Contiene findByIdCorsoAndIdMateria(@PathVariable("idCorso")Long idCorso, @PathVariable("idMateria")Long idMateria) throws NotFoundException, BadRequestException{
		return contieneService.findByIdCorsoAndIdMateria(idCorso, idMateria);
	}
	
	@PostMapping("/contiene")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Contiene addInContiene(@RequestBody Contiene c) throws NotFoundException, BadRequestException, ConflictException {
		return contieneService.save(c);
	}
	
	@PostMapping("/contains/bulk-insert")
    public List<Contiene> createMoreAssociazioni(@RequestBody List<Contiene> c) throws BadRequestException, ConflictException, NotFoundException {
        if(c != null && !c.isEmpty()) 
        contieneService.insertAll(c);
        return c;
    }

	@DeleteMapping("/contiene/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeFromContiene(@PathVariable("id") Long id) throws NotFoundException, BadRequestException,ConflictException{
		contieneService.delete(id);
	}
	
	@GetMapping("/contains")
	@ResponseStatus(HttpStatus.OK)
	public PaginetedResponse<Contiene> findListContains(@RequestParam (required = false)Long idMateria, 
											 			@RequestParam (required = false)Long idCorso,
											 			@RequestParam (required = false)Integer Page,
											 			@RequestParam (required = false)Integer limit,
											 			@RequestParam (required = false)String sort
											 			)throws NotFoundException, BadRequestException {
		return contieneService.findListContains(idMateria, idCorso, limit, limit, sort);
	}

	
	
	
	
	
	
	
	

}

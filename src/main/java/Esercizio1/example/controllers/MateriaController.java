package Esercizio1.example.controllers;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import Esercizio1.example.classes.Materia;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.services.MateriaService;

@RestController
@RequestMapping("/api/materie")
public class MateriaController {

	private final MateriaService materiaService;

	@Autowired
	public MateriaController(MateriaService materiaService) {
		this.materiaService = materiaService;
	}


	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public PaginetedResponse <Materia> list (@RequestParam (required = false)Long id,
											 @RequestParam (required = false)String nomeMateria,
											 @RequestParam (required = false)String codiceMateria,
											 @RequestParam (required = false)String materiaPropedeutica,
											 @RequestParam (required = false)Integer cfu,
											 @RequestParam (required = false)Integer page,
											 @RequestParam (required = false)Integer limit,
											 @RequestParam (required=false) String sort) throws BadRequestException,NotFoundException {

		return materiaService.findList(id,nomeMateria,codiceMateria,materiaPropedeutica,cfu,page,limit,sort);

	}
	
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Materia findById(@PathVariable("id")Long id) throws NotFoundException, BadRequestException  {
		return materiaService.findById(id); 
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Materia createMateria(@RequestBody Materia m) throws BadRequestException, ConflictException {
		return materiaService.save(m);
	}
	
	@PostMapping("/bulk-insert")
    public List<Materia> createMoreMaterie(@RequestBody List<Materia> m) throws BadRequestException, ConflictException {
        if(m != null && !m.isEmpty()) 
        materiaService.insertAll(m);
        return m;
    }
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Materia updateMateria(@PathVariable("id") Long id, @RequestBody Materia m) throws NotFoundException, BadRequestException, ConflictException {
		return materiaService.update(id, m);
	}
	
	@Transactional
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteMateria(@PathVariable("id")Long id) throws NotFoundException {
		materiaService.delete(id);
	}








}













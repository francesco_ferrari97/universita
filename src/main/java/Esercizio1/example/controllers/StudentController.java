package Esercizio1.example.controllers;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Transient;
import javax.transaction.Transactional;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.Student;
import Esercizio1.example.classes.University;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.services.StudentService;

@RestController
@RequestMapping("/api/students")
public class StudentController {
	
	
	private final StudentService studentService;
	
	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public PaginetedResponse <Student> list(@RequestParam (required = false)Long id,
			                                @RequestParam (required = false)String name,
			                                @RequestParam (required = false)String lastName,
			                                @RequestParam (required = false)String matricola,
			                                @RequestParam (required = false)@DateTimeFormat(pattern = "dd/MM/yyyy")LocalDate dataNascita,
			                                @RequestParam (required = false)@DateTimeFormat(pattern = "dd/MM/yyyy")LocalDate dataIscrizione,
											@RequestParam (required = false)Integer page,
											@RequestParam (required = false)Integer limit,
											@RequestParam (required = false)String sort) throws BadRequestException, NotFoundException {
		return studentService.findList(id,name,lastName,matricola,dataNascita,dataIscrizione,page,limit,sort);
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Student findStudent(@PathVariable("id") Long id) throws NotFoundException, BadRequestException {
		return studentService.findById(id);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Student createStudent(@RequestBody Student s) throws BadRequestException, ConflictException, NotFoundException {
		return studentService.save(s);
	}
	
	@Transient
	@PostMapping("/bulk-insert")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Student> createMoreStudent(@RequestBody List<Student>s) throws Exception {
		if(s!=null && !s.isEmpty())
		studentService.insertAll(s);
		return s;
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Student updateStudent(@PathVariable("id") Long id, 
								 @RequestBody Student student) throws NotFoundException, BadRequestException, ConflictException {
		return studentService.update(id, student);
	}
	
	@Transactional
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteStudent(@PathVariable("id") Long id) throws NotFoundException {
		studentService.delete(id);
	}
	
	
	
	

	

}

package Esercizio1.example.controllers;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.classes.State;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.services.StateService;

@RestController
@RequestMapping("/api/states")
public class StateController {

	private final StateService stateService;
	
	public StateController(StateService stateService) {
		this.stateService = stateService;
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public PaginetedResponse <State> list(@RequestParam (required = false)Long id,
			                              @RequestParam (required = false)String name,
			                              @RequestParam (required = false)String code,
			                              @RequestParam (required = false)Long population,
										  @RequestParam (required = false)Integer page,
									      @RequestParam (required = false)Integer limit,
										  @RequestParam (required=false) String sort) throws BadRequestException, NotFoundException {
		return stateService.findList(id,name,code,population,page,limit,sort);
	}

	
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public State findById(@PathVariable("id")Long id) throws NotFoundException, BadRequestException  {
		return stateService.findById(id); 
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public State createState(@RequestBody State s) throws BadRequestException, ConflictException {
		return stateService.save(s);
	}
	
	@PostMapping("/bulk-insert")
    public List<State> createMoreState(@RequestBody List<State> s) throws BadRequestException, ConflictException {
        if(s != null && !s.isEmpty()) 
        stateService.insertAll(s);
        return s;
    }
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public State updateState(@PathVariable("id") Long id, @RequestBody State s) throws NotFoundException, BadRequestException, ConflictException {
		return stateService.update(id, s);
	}
	
	@Transactional
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteState(@PathVariable("id")Long id) throws NotFoundException {
		stateService.delete(id);
	}
	
	
	
	
}
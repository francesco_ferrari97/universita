package Esercizio1.example.controllers;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Transient;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import Esercizio1.example.classes.Libretto;
import Esercizio1.example.classes.PaginetedResponse;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;
import Esercizio1.example.services.LibrettoService;

@RestController
@RequestMapping("/api/libretti")
public class LibrettoController {

	private final LibrettoService librettoService;

	@Autowired
	public LibrettoController(LibrettoService librettoService) {
		this.librettoService = librettoService;
	}


	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public PaginetedResponse <Libretto> list (@RequestParam (required = false)Long id,
										      @RequestParam (required = false)String matricola,
										      @RequestParam (required = false)String codiceMateria,
										      @RequestParam (required = false)String nomeCorso,
										      @RequestParam (required = false)@DateTimeFormat LocalDate dataSostenimento,
										      @RequestParam (required = false)Integer voto,
										   	  @RequestParam (required = false)Integer page,
										   	  @RequestParam (required = false)Integer limit,
										   	  @RequestParam (required=false) String sort) throws BadRequestException,NotFoundException {

		return librettoService.findList(id, matricola, codiceMateria, nomeCorso, dataSostenimento, voto, page, limit, sort);

	}
	
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Libretto findById(@PathVariable("id")Long id) throws NotFoundException, BadRequestException  {
		return librettoService.findById(id); 
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Libretto createLibretto(@RequestBody Libretto l) throws BadRequestException, ConflictException, NotFoundException {
		return librettoService.save(l);
	}
	
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Libretto updateLibretto(@PathVariable("id") Long id, @RequestBody Libretto l) throws NotFoundException, BadRequestException, ConflictException {
		return librettoService.update(id, l);
	}
	
	@Transactional
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteLibretto(@PathVariable("id")Long id) throws NotFoundException {
		librettoService.delete(id);
	}
	
	@Transient
	@PostMapping("/bulk-insert")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Libretto> createMoreRecord(@RequestBody List<Libretto>l) throws BadRequestException, ConflictException, NotFoundException {
		if(l!=null && !l.isEmpty())
		librettoService.insertAll(l);
		return l;
	}
	
	
	
	
}

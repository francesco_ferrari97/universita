package Esercizio1.example.utils;


import org.springframework.data.domain.Sort;

public class Utilities {

	public static Sort buildingSort(String sort) {
		Sort s1=null;
		if(sort!=null) {
			String[]s2=sort.split(",");
			for(String s:s2) {
				Sort.Direction dir=s.contains("-") ? Sort.Direction.DESC : Sort.Direction.ASC;
				if(s1==null) {
					s1=Sort.by(dir,s.replace("-", ""));
				}else {
					s1.and(Sort.by(dir,s.replace("-", "")));
				}
			}
		}
		return s1;
	}

	
}

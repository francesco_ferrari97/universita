package Esercizio1.example.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import Esercizio1.example.exceptions.BadRequestException;
import Esercizio1.example.exceptions.ConflictException;
import Esercizio1.example.exceptions.NotFoundException;

@ControllerAdvice
public class ControllerExceptionHandler {
	
	@ExceptionHandler(value= {NotFoundException.class})
	protected ResponseEntity<ErrorResponse> handleNotFound(NotFoundException ex){
		return new ResponseEntity<>(new ErrorResponse(ex.getMessage()), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value= {BadRequestException.class})
	protected ResponseEntity<ErrorResponse> handleBadRequest(BadRequestException ex){
		return new ResponseEntity<>(new ErrorResponse(ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value= {ConflictException.class})
	protected ResponseEntity<ErrorResponse> handleConflict(ConflictException ex){
		return new ResponseEntity<>(new ErrorResponse(ex.getMessage()), HttpStatus.CONFLICT);
	}
	
	

}

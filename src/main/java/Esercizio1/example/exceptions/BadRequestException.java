package Esercizio1.example.exceptions;

public class BadRequestException extends Exception{
	
	public BadRequestException(String message) {
		super(message);
	}
}

package Esercizio1.example.exceptions;

public class ConflictException extends Exception {
	
	public ConflictException(String message) {
		super(message);
	}

}
